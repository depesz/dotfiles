# env directory is usually the same as $HOME
env_directory="$( dirname "${BASH_SOURCE[0]}" )"

# dotfiles directory is path to where dotfiles were cloned/copied to.
dotfiles_directory="$( dirname "$( readlink -f "${BASH_SOURCE[0]}" )" )"

if [[ -e "$env_directory/.bashrc" ]]
then
    source "$env_directory/.bashrc"
fi

# Helper function to attach to existing SSH Agent socket, in case environment
# got broken
steal_ssh_agent() {
    # Generate data about existing (and accessible) SSH Agent sockets
    # Format is:
    # loaded_key_count socket_creation_time socket_path list_of_loaded_keys_base64_encoded
    # Data is sorted numerically, so last line will have the most keys, and if
    # multiple sockets have the same number of keys - newest socket will be last
    sockets_data="$(
        for socket in /tmp/ssh*/*
        do
            # Gets list of loaded keys from given SSH Socket
            socket_keys="$( SSH_AUTH_SOCK="$socket" timeout 5 ssh-add -l 2> /dev/null )"
            key_count="$( echo "${socket_keys}" | wc -l )"

            # If there are no loaded keys, there is no point in showing it
            (( 0 == "$key_count" )) && continue

            # Get timestamp (seconds from epoch) for when the socket was
            # created (to pick newest one).
            socket_created="$( stat -c '%Y' "$socket" )"

            # Return formatted data
            printf "%d %d %s %s\n" "$key_count" "$socket_created" "$socket" "$( echo "${socket_keys}" | base64 | tr -d '\n' )"
        done | sort -n
        )"

    socket_count="$( echo "${sockets_data}" | wc -l )"

    # Processing makes sense only if there are some keys...
    if (( 0 == "${socket_count}" ))
    then
        echo "There are no available SSH sockets with loaded keys." >&2
    else
        if (( 1 == "${socket_count}" ))
        then
            echo "Available socket:"
        else
            echo "Available sockets:"
        fi

        # i is loop counter, to find out which key is last.
        i=0
        while read key_count socket_created socket keys_data_base64
        do
            (( i++ ))

            # Command that can be used to set this socket.
            cmd="export SSH_AUTH_SOCK=${socket}"

            # Print socket info
            printf '%2d> %-60s # Created: %s\n' "$i" "$cmd" "$( date -d "@${socket_created}" +"%Y-%m-%d %H:%M:%S %Z" )"

            # also print list of keys in this agent
            echo "$keys_data_base64" | base64 -d - | nl | sed 's/^ */  /;s/\t/. /'

            # set env if this is last socket.
            if (( "$i" == "${socket_count}" ))
            then
                $cmd
            else
                echo
            fi
        done <<< "${sockets_data}"
        printf '\nPicked last socket.\n'
    fi
}

# If you will run this function in your .bash_profile-local, when you'll log to the account via ssh, it will automatically start shared screen session.
# Thanks to this - screen will be always started when you work on this account (unless you'll work around not to run it).

screen_as_shell() {
    if [[ ( -z "$STY" ) && ( ! -z "$SSH_CONNECTION" ) ]]
    then
        COUNT="$( LC_ALL=C LANG=C screen -S shell -ls | grep -E '[^[:space:]]' | wc -l )"
        if [[ "$COUNT" -lt 3 ]]
        then
            exec screen -S shell
        fi
        exec screen -S shell -xRR
    fi
}

# Function below works just as screen_as_shell(), but uses tmux instead

tmux_as_shell() {
    if [[ ( -z "$TMUX" ) && ( ! -z "$SSH_CONNECTION" ) && ( -f "$dotfiles_directory/bin/tmux-auto-reattach" ) ]]
    then
        exec "$dotfiles_directory/bin/tmux-auto-reattach" -s -n shell bash -l
    fi
}

_vaulted_whoami() {
    [[ -n "${VAULTED_ENV_ROLE_ARN}" ]] || return
    if [[ -f ~/.rapture/aliases.json ]]
    then
        jq -r --arg arn "${VAULTED_ENV_ROLE_ARN}" 'to_entries[]|select(.value == $arn).key' ~/.rapture/aliases.json
    else
        echo "${VAULTED_ENV_ROLE_ARN}"
    fi
}

_set_prompt_color_helper() {
    printf '\\[\033[%sm\\]%s' "${1:-}" "${2:-}"
}

_set_prompt() {
    local ret=$?

    # Start with resetting ansi colors, and new line character
    local _prompt="$( _set_prompt_color_helper 0 '\n' )"

    (( $ret == 0 )) || _prompt="${_prompt}$( _set_prompt_color_helper "38;5;196" "▲${ret}▲ " )"

    _prompt="${_prompt}$( _set_prompt_color_helper '38;5;244' '\t ' )"

    if (( $UID == 0 ))
    then
        _prompt="${_prompt}$( _set_prompt_color_helper '1;33;41' '\u@\h' )"
    else
        _prompt="${_prompt}$( _set_prompt_color_helper '0;32' '\u@\h' )"
    fi

    # reset colors to avoid overflowing background
    _prompt="${_prompt}$( _set_prompt_color_helper 0 '' )"

    if [[ -n "${SSH_AUTH_SOCK}" ]]
    then
        key_count="$( ssh-add -l 2> /dev/null | grep -c '^[1-9]' )"
        if (( "${key_count}" > 0 ))
        then
            _prompt="${_prompt}$( _set_prompt_color_helper '38;5;22' ' 🔑:' )"
            _prompt="${_prompt}$( _set_prompt_color_helper '38;5;28' "${key_count}" )"
        else
            _prompt="${_prompt}$( _set_prompt_color_helper '38;5;58' ' 🔑:' )"
            _prompt="${_prompt}$( _set_prompt_color_helper '38;5;124' "0" )"
        fi
    else
        _prompt="${_prompt}$( _set_prompt_color_helper '38;5;124' ' ❗🔑' )"
    fi

    # Add information about loaded ssh agent
    if [[ -d ~/.ssh/agents ]]
    then
        local _loaded=""
        local _hostname="$( hostname -s )"
        local _comma="$( _set_prompt_color_helper '38;5;22' ',' )"
        local i

        for i in ~/.ssh/agents/agent-*
        do
            # Get agent name
            local _short_name="${i##*/agents/agent-}"
            _short_name="${_short_name//git/🐙}"
            [[ "${_short_name}" == 'instructure' ]] && _short_name='inst'

            # Ignore socket files
            [[ "${i}" == *.socket ]] && continue

            # Make sure socket exists for this agent
            local _agent_sock="${i}.socket"
            [[ -S "${_agent_sock}" ]] || continue

            # Check if agent pid exists
            local _agent_pid="$( sed -ne 's/^echo Agent pid \([0-9]\+\);$/\1/p' "$i" )"
            [[ -n "${_agent_pid}" ]] || continue
            ps -p "${_agent_pid}" &> /dev/null || continue

            # Count laoded keys
            local _loaded_keys="$( SSH_AUTH_SOCK="${_agent_sock}" timeout 2 ssh-add -l 2>&1 | grep -cE "^[0-9]" )"

            # Add current agent info to _loaded
            [[ -n "${_loaded}" ]] && _loaded="${_loaded}${_comma}"
            local _color=28
            [[ "${_agent_sock}" == "${SSH_AUTH_SOCK:-}" ]] && _color=40
            if (( 0 == "${_loaded_keys}" ))
            then
                _loaded="${_loaded}$( _set_prompt_color_helper "38;5;${_color}" "${_short_name}:" )"
                _loaded="${_loaded}$( _set_prompt_color_helper '38;5;124' "${_loaded_keys}:" )"
            else
                _loaded="${_loaded}$( _set_prompt_color_helper "38;5;${_color}" "${_short_name}:${_loaded_keys}" )"
            fi
        done 2> /dev/null
        if [[ -n "${_loaded}" ]]
        then
            _prompt="${_prompt}$( _set_prompt_color_helper '38;5;22' ' 😎🔑:' )"
            _prompt="${_prompt}${_loaded}"
        else
            _prompt="${_prompt}$( _set_prompt_color_helper '38;5;124' ' ❗😎🔑' )"
        fi
    fi

    local gitref="$( git symbolic-ref HEAD 2> /dev/null || true )"
    if [[ -n "${gitref}" ]]
    then
        local branch_name="${gitref##*/}"
        _prompt="${_prompt}$( _set_prompt_color_helper '0;38;5;26' " git:" )"
        _prompt="${_prompt}$( _set_prompt_color_helper '38;5;33' "${branch_name}" )"
    fi

    if [[ -n "${VAULTED_ENV}" ]]
    then
        _prompt="${_prompt}$( _set_prompt_color_helper '38;5;54' " AWS:" )"
        _prompt="${_prompt}$( _set_prompt_color_helper '38;5;126' "${VAULTED_ENV}" )"
        if [[ -n "${VAULTED_ENV_ROLE_ARN}" ]]
        then
            _prompt="${_prompt}$( _set_prompt_color_helper '38;5;54' ">" )"
            _prompt="${_prompt}$( _set_prompt_color_helper '38;5;93' "$( _vaulted_whoami )" )"
        fi
    fi

    _prompt="${_prompt}$( _set_prompt_color_helper '38;5;173' ' \w' )"
    _prompt="${_prompt}$( _set_prompt_color_helper 0 '\n=\$ ' )"

    export PS1="${_prompt}"
}

export PROMPT_COMMAND=_set_prompt
export PS4="$( _set_prompt_color_helper 0 '\n' )$( _set_prompt_color_helper '38;5;88' 'bash +' )$( _set_prompt_color_helper 0 )"

# Helper function to load ssh identities
ssh_identity() {
    local _identity="${1:-}"
    local _env_file="${HOME}/.ssh/agents/agent-${_identity}"
    if [[ -f "${_env_file}" ]]
    then
        source "${_env_file}" &> /dev/null
        echo "SSH identity is set to ${_identity}."
    else
        echo "There is no agent file for identity [${_identity}] : [${_env_file}]" >&2
    fi
}

# Enable per-directory env files
cd () { builtin cd "$@" && _load_bash_per_dir_env; }
pushd () { builtin pushd "$@" && _load_bash_per_dir_env; }
popd () { builtin popd "$@" && _load_bash_per_dir_env; }
_load_bash_per_dir_env () {
    local _pwd="$( pwd -P )"
    while true
    do
        [[ "${_pwd}" == "${BASH_DIR_ENV}" ]] && break
        if [[ "${_pwd}" == '/' ]]
        then
            local _env_file='/.bash_dir.env'
        else
            local _env_file="${_pwd}/.bash_dir.env"
        fi
        if [[ -f "${_env_file}" ]]
        then
            export BASH_DIR_ENV="${_pwd}"
            . "${_env_file}"
            break
        fi
        [[ "${_pwd}" == '/' ]] && break
        _pwd="$( dirname "${_pwd}" )"
    done
}

# Turn off flow control. It's XXI century, and our terminals have scrollbar.
stty -ixon -ixoff 2> /dev/null
# Turn off flow control. It's XXI century, and our terminals have scrollbar.

if [[ -f "${HOME}/.dircolors" ]]
then
    eval "$( dircolors -b "${HOME}/.dircolors" )"
else
    export LS_COLORS="rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2ts=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.m3u=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:"
fi

if [[ -e "$env_directory/.bash_profile-local" ]]
then
    source "$env_directory/.bash_profile-local"
fi
